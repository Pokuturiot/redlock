from redlock import RedLock
# By default, if no redis connection details are
# provided, RedLock uses redis://127.0.0.1:6379/0
lock =  RedLock("distributed_lock")
lock.acquire()
do_something()
lock.release()
